import React from 'react';
import logo from './assets/Vector2.png';
import first from './assets/First.png';
import burger from './assets/burger.png';

const Navbar = () => {
    return (
        <>
            <navbar className="navbar">
                <img src={burger} alt="burger menu" className="navbar__burger"/>
                <ul className="navbar__list">
                    <li>Home</li>
                    <li>About</li>
                    <li>Areas</li>
                    <li>Cases</li>
                    <img src={logo} alt="company logo" />
                    <li>Users</li>
                    <li>Reviews</li>
                    <li>Download</li>
                    <li>Contacts</li>
                </ul>
                <section className="navbar__achievements">
                    <img src={first} alt="first" />
                    <h5>TOP AI COMPANY</h5>
                    <h5>TOP AI EXECUTIVE</h5>
                </section>
            </navbar>
        </>
    );
}

export default Navbar;