import React from 'react';

const Main = () => {
    return (
        <>
            <section className="main">
                <h1 className="main__topic">Privacy Policy</h1>
                <section className="main__article">
                    <acrticle className="main__article__content">
                        <h2>About</h2>
                        <p>The Website is operated and the property of <span className="main__article__content__highlight">COMPANY</span> hereinafter referred to as Company, incorporated in <span className="main__article__content__highlight">NUMBER</span> whose registered office at ADDRESS. In this document, references to “we”, “us” or “our” related only to Company.
                            <br />
                            <br />
                            This Privacy Policy applies to your use of <a href="https://www.gargoyle.ltd/" className="main__article__content__highlight">https://www.gargoyle.ltd/</a> hereinafter referred to as the Website. It describes to you hereinafter referred to as the Visitor what to expect when we collect personally identifiable information about you and what your rights are concerning the collection of your information.
                            <br />
                            <br />
                            The information on the present website is the subject of a system for the protection of personal data and a policy of respect for privacy in electronic communication, provided in accordance with our obligations under the European Union General Data Protection Regulation.
                            <br />
                            <br />
                            We will not collect any personally identifiable information about you unless it is in response to you using our Website. The security of your personally identifiable information is extremely important to us. Please read our Privacy Policy carefully as it will help you make informed decisions about sharing your personal information with us.
                            <br />
                            <br />
                            This Privacy Policy is regularly reviewed to make sure that we continue to serve your privacy interests. We reserve the right to update it as we deem necessary.
                        </p>
                    </acrticle>
                    <article className="main__article__content">
                        <h2>Data Collection</h2>
                        <p>You can browse our Website as a Visitor to find out more about our Company and you are not required to provide us with any personal information in that case. We collect your personal information in case when you fulfill the form on our Website in order to obtain more information about us, our services, our products or otherwise contact us. When you send the information on the Website, we and third parties working with us will collect the following information: full name, email address, phone number, company name, or other information you provide.
                            <br />
                            <br />
                            When you access the Website we may use cookies to track, collect, and aggregate information about your use of the Website, including pages you have visited, the specific Uniform Resource Locators that brought you to our Website, and links embedded on our Website that you have accessed. This information may include Internet Protocol addresses and any information collected by cookies.  This may also include information such as device used for visit, visited website pages, session time and length, activities on visited pages, other information cookies provide.
                            <br />
                            <br />
                            When you subscribe to our updates via any form on our Website such as a download form or form on the blog page, we collect the email address you provide and your subscription preferences.
                            <br />
                            <br />

                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Data Usage</h2>
                        <p>We and third parties we work with will collect, store, and use your personal information in order to provide you with information. If you have subscribed to our updates, we will use the email address you provided to send information about our services, recent updates, latest products, case studies, and research.
                            <br />
                            <br />
                            We collect your data to contact you occasionally in order to invite you to share your opinions of and experiences with the Website and to publish customer reviews and ratings of our services on our Website or on other media channels.
                            <br />
                            <br />
                            We collect your data for marketing purposes, where you provide us with your consent to receive marketing communications, which may include email and telephone communications with information, news, and offers of services on our Website and third-party offers.
                            <br />
                            <br />
                            We collect your data to use your information in an aggregated format to identify trends on our website and trends in our customer database. We may use this information in an aggregated and anonymous format and may share this information with third parties.
                            <br />
                            <br />
                            We use your data for establishing business relations. If you are an existing customer of the Company or we reasonably believe that you might want to be our customer, we will use your email address, phone number, and other contact information you provide us by written or oral means for establishing a business relationship, sending you information about our services, and providing you with updates that may be related to your personal and professional interests.
                            <br />
                            <br />
                            Also, we use cookies to collect information about your usage of the Website in order to enable, maintain and improve the quality of our services and Website.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Data Security</h2>
                        <p>All information is stored on our secure servers. We securely store your data at Amazon Relational Database Service on a database instance running with encryption. Data stored at rest in the underlying storage is encrypted, as are its automated backups, read replicas, and snapshots that use the industry-standard AES-256 encryption algorithm to encrypt your data on the server that hosts the instance. Your data will be stored until the moment you withdraw your consent or our business relationship is broken off, whichever happens later.
                            <br />
                            <br />
                            Also, we and third parties acting on our behalf may also store or process information that we collect about you in countries outside the European Economic Area, which may have different standards of data protection than in the specified area.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Data Transfer</h2>
                        <p>For the above purposes, we may share your information with the following third parties. Please note that any third party’s use of your personal data will be governed by their own privacy policies and give rise to separate obligations under applicable data protection laws. We suggest that you carefully review any third-party privacy policies that may apply to you. Third parties that may receive your information include payment services providers, legal or governmental authorities, analytical tools that give us information about user behavior such as platforms and browsers used to access the Website and Subdomains, pages visited on the Website and Subdomains, the region from which a visitor is accessing the Website and Subdomains, how a visitor accessed our Website and Subdomains, and other behavioral information.
                            <br />
                            <br />
                            In addition, your data may also be transferred to subcontractors. During this process, data may be transferred to parties located in countries outside European Union that are not bound by European Union data protection regulations. However, to protect your data, we always sign contracts with our subcontractors that contain provisions for the protection of user data.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Data Retention</h2>
                        <p>We retain personal information we collect from you where we have an ongoing legitimate business need to do so. For example, to provide you with a service you have requested or to comply with applicable legal, tax, or accounting requirements.
                            <br />
                            <br />
                            When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it or, if this is not possible, then we will securely store your personal information and isolate it from any further processing until deletion is possible.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Legal Basis</h2>
                        <p>We have several legal bases for collecting and using personal information where the use of your personal information is necessary to perform our obligations under any contract with you, where we have your explicit consent to collect, store, and use your information for a particular purpose, including where you consent to receive marketing and promotional communications for services on our Website or third-party websites.
                            <br />
                            <br />
                            Also, we collect your data in cases where the use of your personal information is necessary for purposes of pursuing the Company’s legitimate interests or the legitimate interests of a third party as long as these interests are not outweighed by your own interests, fundamental rights, or freedoms.  For example, we will need to ensure that in these cases you would reasonably expect your personal information to be used and that it would have a minimal impact on your personal privacy.
                            <br />
                            <br />
                            Such legitimate interests include cases where we need to ensure we are delivering a high level of customer service through ongoing customer communications and responses to customer queries, to invest, test, and roll out new services which we believe will benefit our customers or prospective customers, track and ensure completion of contractual obligations, carry out market research and business development, to operate our website, and in some cases, where we may have a legal obligation to collect personal information from you.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Your Rights</h2>
                        <p>Regarding your rights, you can edit your personal details by contacting us whenever you wish.  We maintain a procedure in order to help you confirm that your personal information remains correct and up-to-date or choose whether or not you wish to receive material from us or some of our partners.
                            <br />
                            <br />
                            In addition, you can object to the processing of your personal information, ask us to restrict the processing of your personal information, or request the portability of your personal information. Also, you have a right to erasure known as "the right to be forgotten" which means that under certain circumstances you can ask for your personal data to be deleted. Besides, you have the right not to be subject to a decision based solely on automated processing, including profiling. You have the right to be informed which describes how and why your personal data are being processed. Finally, you have the right to obtain confirmation as to whether or not personal data concerning you are being processed, and, where that is the case, access to the personal data. Again, you can exercise these rights by contacting us using the contact details provided under the “Contact details” heading below.
                            <br />
                            <br />
                            Similarly, if we have collected and processed your personal information on the basis of your consent, then you can withdraw your consent at any time. Withdrawing your consent will not affect the lawfulness of any processing we conducted prior to your withdrawal, nor will it affect the processing of your personal information conducted in reliance on lawful processing grounds other than consent.
                            <br />
                            <br />
                            If you have any privacy-related questions or unresolved problems, you may contact us using the information provided below. Also, you have the right to complain to a data protection authority about our collection and use of your personal information. For more information, please contact your local data protection authority in the European Economic Area.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Cookies Usage</h2>
                        <p>Cookies are small text files that, subject to your choice, are downloaded to your device (computer, smartphone, tablet computer) when you visit a website. The term cookie has to be interpreted in the broadest sense and covers all the tracers downloaded and/or read, for example, when you visit a website, read an email, install software, or a mobile application. Cookies are managed by your web browser and only the cookie issuer can read or modify the information included in it.
                            <br />
                            <br />
                            They are essential to the effective operation of our website and to make it intuitive. They only concern the way the website functions and improve your browsing by memorizing the information and the data you record to facilitate your access to reserved or personal spaces. Their aim is to collect information about your web browsing and offer you adapted services and adapted publicity for you and for your device. They personalize the content of the site to offer you the best browsing experience possible when you visit our website.
                            <br />
                            <br />
                            Our cookies do not store personal information such as your name, address, phone number, or email in a format that can be read by others. The cookies we use cannot read or search your computer, smartphone, or web-enabled device to obtain information about you or your family or read any material kept on your hard drive.
                            <br />
                            <br />
                            If you follow a link from our website to another website, please be aware that the owner of the other website will have their own privacy and cookie policies for their site. We recommend you read their policies as we are not responsible or liable for what happens on their website.
                            <br />
                            <br />
                            We use strictly necessary cookies that are essential for our online services and tools to function and to enable you to use their distinct functionalities. They collect or record information that we need to make our website work. These cookies only last for a single browsing session — when you leave our website, they are removed.
                            <br />
                            <br />
                            We use functionality cookies that allow us to provide you with enhanced features that need to remember your preferences and choices to help us offer them to you when you return and to adapt the pages’ presentation to your device’s predilection display. They will enhance your browsing on our website.
                            <br />
                            <br />
                            We use performance cookies that help us monitor and improve how our website works collecting anonymous information when you visit our website (the way you arrived on the site, pages you go to most often). They also let us monitor how our website is performing so we can keep on improving it. These cookies are stored until an expiry date fixed by the website or your browser.
                            <br />
                            <br />
                            We use cookies facilitating online communication that aims to remember the data entered by you on the website to, for example, facilitate accurate quotations, online simulations, and purchase of our services. Also, we use cookies “social buttons” that allow sharing of content of their site on social platforms. But the source code available to the site designers uses cookies to trace browsing the Internet, whether they are or are not users.
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Contact Details</h2>
                        <p>We have our own Data Protection Officer in the person of <span className="main__article__content__highlight">NAME</span> who is responsible for all issues connecting with data processing. Any Visitor may, at any time, contact our Data Protection Officer directly with all questions, claims, and suggestions concerning data protection by the following email address: <span className="main__article__content__highlight">EMAIL</span>
                        </p>
                    </article>
                    <article className="main__article__content">
                        <h2>Policy Updates</h2>
                        <p>We may update or amend this Privacy Policy from time to time, to comply with the law, in accordance with best practice in our industry, or to meet our changing business requirements. Any updates or amendments will be posted on the Website. By continuing to access the Website, use the Content, and provide information in connection with the Content, you will be deemed to have accepted the Privacy Policy as amended at the time of your access and use.
                        </p>
                    </article>
                </section>
                <h1 className="main__filler">{' '}</h1>
            </section>
        </>
    );
}

export default Main;